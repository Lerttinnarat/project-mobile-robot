#!/usr/bin/env node

// Connecting to ROS 
var ROSLIB = require('roslib');

var admin = require("firebase-admin");

var serviceAccount = require("./project-mobile-robot-firebase-adminsdk-6b79m-d0ee3b431a.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://project-mobile-robot.firebaseio.com/"
});

var status_open = admin.database().ref(`door`);
var battery_percent = admin.database().ref(`battery`); 
var robot_arrive = admin.database().ref(`arrive`); 

var ros = new ROSLIB.Ros({
    url : "ws://localhost:9090"
});

ros.on('connection', function() {
console.log("Connected to websocket server.");
});

ros.on('error', function(error) {
console.log('Error connecting to websocket server: ', error);
});

ros.on('close', function() {
console.log('Connection to websocket server closed.');
});

var getData = new ROSLIB.Topic({
  ros : ros,
  name : '/get_Data',
  messageType : 'std_msgs/Bool'
});

var door = new ROSLIB.Topic({
  ros : ros,
  name : '/door',
  messageType : 'std_msgs/Bool'
});

var return_home = new ROSLIB.Topic({
  ros : ros,
  name : '/return_home',
  messageType : 'std_msgs/Bool'
});

var battery = new ROSLIB.Topic({
  ros : ros,
  name : '/screen/batt',
  messageType : 'std_msgs/Float32'
});

var arrive = new ROSLIB.Topic({
  ros : ros,
  name : '/arrive',
  messageType : 'geometry_msgs/Twist'
});

var location = new ROSLIB.Topic({
  ros : ros,
  name : '/location',
  messageType : 'geometry_msgs/Twist'
});


battery.subscribe(function(val) {
  console.log(val.data);
  battery_percent.update({
    percent : val.data
  });
});

arrive.subscribe(function(velocity) {
  console.log(velocity.linear);
  robot_arrive.update({
      x : velocity.linear.x,
      y : velocity.linear.y,
      z : velocity.linear.z
  });
});


getData.subscribe(function(data) {
  var open = admin.database().ref('door');
  open.on("value", function(snapshot) {
    door.publish(snapshot.val());
    console.log("send open");
  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
  });  

// New Update
  // topic name = return_home 
  var open = admin.database().ref('return_home');
  open.on("value", function(snapshot) {
    return_home.publish(snapshot.val());
    console.log("send return_home");
  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
  });  

  // topic name = location
  var open = admin.database().ref('location');
  open.on("value", function(snapshot) {
    location.publish({
      linear : {
        x : snapshot.val().x,
        y : snapshot.val().y
      }
    });
  }, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
  });  
});
